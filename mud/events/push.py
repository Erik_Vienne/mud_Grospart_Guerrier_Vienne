# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3

class PushEvent(Event2):
    NAME = "push"

    def perform(self):
        if not self.object.has_prop("pushable"):
            self.fail()
            return self.inform("push.failed")
        self.inform("push")

class PushWithEvent(Event3):
    NAME = "push-with"

    def perform(self):
        if not self.object2.has_prop("pushable-with"):
            self.add_prop("object2-not-pushable-with")
            return self.push_with_failure()
        if self.object not in self.actor:
            self.add_prop("object-not-in-inventory")
            return self.push_with_failure()
        self.inform("push-with")
    
    def push_with_failure(self):
        self.fail()
        self.inform("push-with.failed")