# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class UpEvent(Event2):
    NAME = "up"

    def perform(self):
        if not self.object.has_prop("can-up"):
            self.fail()
            return self.inform("up.failed")
        self.inform("up")
