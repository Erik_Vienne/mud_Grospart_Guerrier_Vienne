# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class DownEvent(Event2):
    NAME = "down"

    def perform(self):
        if not self.object.has_prop("can-down"):
            self.fail()
            return self.inform("down.failed")
        self.inform("down")
