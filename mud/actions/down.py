# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import DownEvent

class DownAction(Action2):
    EVENT = DownEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "down"
