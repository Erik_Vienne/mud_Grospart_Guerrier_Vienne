# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import UpEvent

class UpAction(Action2):
    EVENT = UpEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "up"
