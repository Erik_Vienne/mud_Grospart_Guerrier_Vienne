# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2, Action3
from mud.events import PushEvent, PushWithEvent

class PushAction(Action2):
    EVENT = PushEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "push"

class PushWithAction(Action3):
    EVENT = PushWithEvent
    RESOLVE_OBJECT = "resolve_for_use"
    RESOLVE_OBJECT2 = "resolve_for_operate"
    ACTION = "push-with"